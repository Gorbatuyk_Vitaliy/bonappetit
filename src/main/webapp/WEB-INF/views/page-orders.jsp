<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Bon Appetit</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/icomoon-social.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../../css/leaflet.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="../../css/leaflet.ie.css"/>
    <![endif]-->
    <link rel="stylesheet" href="../../css/main-orange.css">

    <script src="../../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>


<!-- Navigation & Logo-->
<%@ include file="Navigation.jsp" %>
<!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><spring:message code="lang.yourOrders" text="default"/>:</h1>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <c:forEach var="order" items="${orders}">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Shopping Cart Items -->
                    <div class="shop-item">
                        <table class="table">
                            <tr>
                                <td><b> <spring:message code="lang.dateOrder" text="default"/> </b></td>
                                <td><b> <spring:message code="lang.deliveryAddress" text="default"/></b></td>
                                <td><b> <spring:message code="lang.phone" text="default"/></b></td>
                                <td><b> <spring:message code="lang.yourListDish" text="default"/> </b></td>
                                <td><b> <spring:message code="lang.totalPrice" text="default"/> </b></td>
                                <td><b> <spring:message code="lang.orderStatus" text="default"/> </b></td>
                            </tr>
                            <tr>
                                <td> ${order.dateOfOrder}</td>
                                <td> ${order.deliveryAddress}</td>
                                <td> ${order.phone}</td>
                                <td><c:forEach items="${order.dishList}" var="items">
                                    - ${items.name}<br>
                                </c:forEach></td>
                                <td> ${order.totalPrice}</td>
                                <td> ${order.orderStatus}</td>
                            </tr>
                        </table>
                    </div>
                    <!-- End Shopping Cart Items -->
                </div>
            </div>

        </div>
    </c:forEach>
</div>
</div>

<!-- Footer -->
<%@ include file="footer.jsp" %>

<!-- Javascripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../js/jquery-1.9.1.min.js"><\/script>')</script>
<script src="../../js/bootstrap.min.js"></script>
<script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
<script src="../../js/jquery.fitvids.js"></script>
<script src="../../js/jquery.sequence-min.js"></script>
<script src="../../js/jquery.bxslider.js"></script>
<script src="../../js/main-menu.js"></script>
<script src="../../js/template.js"></script>

</body>
</html>