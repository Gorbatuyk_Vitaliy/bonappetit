<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-footer col-md-3 col-xs-6">
                <h3><spring:message code="lang.dishChef" text="default"/></h3>
                <div class="portfolio-item">
                    <div class="portfolio-image">
                        <a href="#"><img src="../../img/dietary-menu-healthy.jpg" alt="Bon Apetit"></a>
                    </div>
                </div>
            </div>
            <div class="col-footer col-md-3 col-xs-6">
                <h3><spring:message code="lang.navigate" text="default"/></h3>
                <ul class="no-list-style footer-navigate-section">
                    <li><a href="#"><spring:message code="lang.blog" text="default"/></a></li>
                    <li><a href="#"><spring:message code="lang.portfolio" text="default"/></a></li>
                    <li><a href="#">Services<spring:message code="lang.services" text="default"/></a></li>
                    <li><a href="#">FAQ</a></li>
                </ul>
            </div>

            <div class="col-footer col-md-4 col-xs-6">
                <h3><spring:message code="lang.contacts" text="default"/></h3>
                <p class="contact-us-details">
                    <b><spring:message code="lang.address" text="default"/>:</b> 123 Fake Street, LN1 2ST, London, United Kingdom<br/>
                    <b><spring:message code="lang.phone" text="default"/>:</b> +44 123 654321<br/>
                    <b><spring:message code="lang.email" text="default"/>:</b> <a href="mailto:vitaljiano@gmail.com">vitalijano@gmail.com</a>
                </p>
            </div>
            <div class="col-footer col-md-2 col-xs-6">
                <h3><spring:message code="lang.stayConnected" text="default"/></h3>
                <ul class="footer-stay-connected no-list-style">
                    <li><a href="#" class="facebook"></a></li>
                    <li><a href="#" class="twitter"></a></li>
                    <li><a href="#" class="googleplus"></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="footer-copyright">&copy; 2021 BonAppetit. Made by Vitalii Horbatiuk.</div>
            </div>
        </div>
    </div>
</div>
