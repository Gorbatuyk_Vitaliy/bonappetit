<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/icomoon-social.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../../css/leaflet.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="../../css/leaflet.ie.css"/>
    <![endif]-->
    <link rel="stylesheet" href="../../css/main-orange.css">

    <script src="../../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>


<%@ include file="Navigation.jsp" %>

<!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><spring:message code="lang.login" text="default"/></h1>
            </div>
        </div>
    </div>
</div>

<div class="s">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <div class="basic-login">
                    <form role="form" role="form" modelAttribute="user" action="<c:url value='/j_spring_security_check'/>" method='POST'>
                        <div class="form-group">
                            <label for="username"><i class="icon-user"></i> <b><spring:message code="lang.email" text="default"/></b></label>
                            <input class="form-control" id="username" name="username" type="text" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="password"><i class="icon-lock"></i> <b><spring:message code="lang.password" text="default"/></b></label>
                            <input class="form-control" id="password" name="password" type="password" placeholder="">
                        </div>
                        <c:if test="${param.error ne null}">
                            <div class="alert-danger"><spring:message code="lang.error.userPass" text="default"/></div>
                        </c:if>

                        <div class="form-group">
                            <div class="not-member">
                                <p><spring:message code="lang.register" text="default"/> <a href="register"><spring:message code="lang.link.register" text="default"/></a></p>
                            </div>
                            <button type="submit" class="btn pull-right"><spring:message code="lang.login" text="default"/></button>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}"
                               value="${_csrf.token}" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>

<!-- Javascripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../js/jquery-1.9.1.min.js"><\/script>')</script>
<script src="../../js/bootstrap.min.js"></script>
<script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
<script src="../../js/jquery.fitvids.js"></script>
<script src="../../js/jquery.sequence-min.js"></script>
<script src="../../js/jquery.bxslider.js"></script>
<script src="../../js/main-menu.js"></script>
<script src="../../js/template.js"></script>

</body>
</html>
