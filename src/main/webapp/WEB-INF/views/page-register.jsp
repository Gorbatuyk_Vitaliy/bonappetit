<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE >

 <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Bon Appetit</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

		<link rel="stylesheet" href="../../css/bootstrap.min.css">
		<link rel="stylesheet" href="../../css/icomoon-social.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" href="../../css/leaflet.css" />
		<!--[if lte IE 8]>
		<link rel="stylesheet" href="../../css/leaflet.ie.css" />
		<![endif]-->
		<link rel="stylesheet" href="../../css/main-orange.css">

		<script src="../../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	</head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

		<%@ include file="Navigation.jsp" %>

        <!-- Page Title -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1><spring:message code="lang.register" text="default"/></h1>
					</div>
				</div>
			</div>
		</div>
        
        <div class="">
	    	<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<div class="basic-login">
							<form:form method="POST" modelAttribute="user" class="form-signin" action="register">
                                <spring:bind path="name">
                                    <div class="form-group" >
		        				 	<label for="register-useremail" ${status.error ? 'has-error' : ''}><i class="icon-user"></i> <b><spring:message code="lang.email" text="default"/></b></label>
									<form:input  class="form-control"  path="name" id="register-useremail" type="text" placeholder=""></form:input>
										<div class="alert-danger"><form:errors path="name"></form:errors></div>
								</div>
                                </spring:bind>
                                <spring:bind path="email">
								<div class="form-group">
									<label for="register-username" ${status.error ? 'has-error' : ''}><i class="icon-user"></i> <b><spring:message code="lang.name" text="default"/></b></label>
										<form:input class="form-control" path="email" id="register-username" type="text" placeholder=""></form:input>
									<div class="alert-danger"><form:errors path="email" cssClass="error"></form:errors></div>
								</div>
                                </spring:bind>
                                <spring:bind path="password">
								<div class="form-group" >
		        				 	<label for="register-password" ${status.error ? 'has-error' : ''}><i class="icon-lock"></i> <b><spring:message code="lang.password" text="default"/></b></label>
										<form:input class="form-control" path="password" id="register-password" type="password" placeholder=""></form:input>
									<div class="alert-danger"><form:errors path="password" cssClass="error"></form:errors></div>
									</spring:bind>
								</div>
                                <spring:bind path="confirmPassword">
								<div class="form-group" >
		        				 	<label for="confirn-password" ${status.error ? 'has-error' : ''}><i class="icon-lock"></i> <b><spring:message code="lang.Re-enterPassword" text="default"/></b></label>
									<form:input class="form-control" path="confirmPassword" id="confirn-password" type="password" placeholder=""></form:input>
									<div class="alert-danger"><form:errors path="confirmPassword" cssClass="error"></form:errors>
								</div>
                                </spring:bind>
								<div class="form-group">
									<button type="submit" class="btn pull-right"><spring:message code="lang.register" text="default"/></button>
									<div class="clearfix"></div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>

	    <!-- Footer -->
		<%@ include file="footer.jsp" %>

        <!-- Javascripts -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../js/jquery-1.9.1.min.js"><\/script>')</script>
		<script src="../../js/bootstrap.min.js"></script>
		<script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
		<script src="../../js/jquery.fitvids.js"></script>
		<script src="../../js/jquery.sequence-min.js"></script>
		<script src="../../js/jquery.bxslider.js"></script>
		<script src="../../js/main-menu.js"></script>
		<script src="../../js/template.js"></script>

    </body>
</html>