<%--
  Created by IntelliJ IDEA.
  User: Vitalik
  Date: 18.05.2021
  Time: 18:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Homepage Slider -->
<div class="homepage-slider">
    <div id="sequence">
        <ul class="sequence-canvas">
            <!-- Slide 1 -->
            <li class="bg1">
                <!-- Slide Title -->
                <h2 class="title"><spring:message code="lang.slider.main1" text="default"/> Bon Appetit</h2>
                <!-- Slide Text -->
                <h3 class="subtitle"><spring:message code="lang.slider.underMain1" text="default"/></h3>
                <!-- Slide Image -->
                <img class="slide-img" src="../../img/homepage-slider/slider-bg4.jpg" alt="Slide 1" />
            </li>
            <!-- End Slide 1 -->
            <!-- Slide 2 -->
            <li class="bg2">
                <!-- Slide Title -->
                <h2 class="title"><spring:message code="lang.slider.main2" text="default"/></h2>
                <!-- Slide Text -->
                <h3 class="subtitle"><spring:message code="lang.slider.underMain2" text="default"/></h3>
                <!-- Slide Image -->
                <img class="slide-img" src="../../img/homepage-slider/slider-bg5.jpg" alt="Slide 2" />
            </li>
            <!-- End Slide 2 -->
            <!-- Slide 3 -->
            <li class="bg3">
                <!-- Slide Title -->
                <h2 class="title"></h2>
                <!-- Slide Text -->
                <h3 class="subtitle"></h3>
                <!-- Slide Image -->
                <img class="slide-img" src="../../img/homepage-slider/slider-bg6.jpg" alt="Slide 3" />
            </li>
            <!-- End Slide 3 -->
        </ul>
        <div class="sequence-pagination-wrapper">
            <ul class="sequence-pagination">
                <li>1</li>
                <li>2</li>
                <li>3</li>
            </ul>
        </div>
    </div>
</div>
<!-- End Homepage Slider -->
