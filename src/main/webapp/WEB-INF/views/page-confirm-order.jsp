<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>BonAppetit - Confirm Order</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/icomoon-social.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../../css/leaflet.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="../../css/leaflet.ie.css"/>
    <![endif]-->
    <link rel="stylesheet" href="../../css/main-orange.css">

    <script src="../../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->


<!-- Navigation & Logo-->
<%@ include file="Navigation.jsp" %>
<!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><spring:message code="lang.confirmOrder" text="default"/></h1>
            </div>
        </div>
    </div>
</div>

<div class="">
    <div class="row">
        <form method="post" action="confirmPay">
            <div class="col-md-12">
                <!-- Shopping Cart Items -->

                <table class="shopping-cart">
                    <!-- Shopping Cart Item -->
                    <tr>
                        <!-- Shopping Cart Item Image -->
                        <td class="image"><a href="page-product-details.jsp"><img src="img/service-icon/diamond.png"
                                                                                  alt="Item Name"></a></td>
                        <!-- Shopping Cart Item Description & Features -->
                        <td>
                            <div class="cart-item-title"><a href="page-product-details.jsp">
                                <h3>${dishes.dish.name}</h3></a></div>
                            <div class="feature color">
                                <div class="h5"><b><spring:message code="lang.yourDish" text="default"/>: </b><c:forEach items="${user.userCart.cartItems}" var="dish">
                                    <div clss="h6">${dish.dish.name}</div><div> <spring:message code="lang.count" text="default"/>: ${dish.countOfDish}</div>
                                </c:forEach></div>
                            </div>

                            <div class="feature color">
                                <div class="h5"><b><spring:message code="lang.yourName" text="default"/>: </b>${user.name}</div>
                            </div>
                            <div class="feature">
                                <div class="h5"><b><spring:message code="lang.yourAdrressDelivery" text="default"/>: </b>${user.userCart.deliveryAddress.deliveryAddress}</div>
                            </div>
                            <div class="feature">
                                <div class="h5"><b><spring:message code="lang.yourContactPhone" text="default"/>: </b>${user.userCart.deliveryAddress.phoneNumber}</div>
                            </div>
                        </td>

                        <td class="price"><spring:message code="lang.totalPrice" text="default"/>: $${user.userCart.totalPrice}</td>
                        <!-- Shopping Cart Item Actions -->
                        <td class="actions">
                            <a href="returnOnPageCart"
                               class="btn btn-xs btn-grey"><i
                                    class="glyphicon glyphicon-trash"><spring:message code="lang.btn.cancel" text="default"/></i></a>
                        </td>
                        <td>
                            <!-- Action Buttons -->
                            <div class="pull-right">
                                <button class="btn"><i class="glyphicon glyphicon-shopping-cart icon-white"></i><spring:message code="lang.btn.confirm" text="default"/>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <!-- End Shopping Cart Item -->
                </table>
                <!-- End Shopping Cart Items -->
            </div>

        </form>
    </div>
</div>

<!-- Footer -->
<%@ include file="footer.jsp" %>

<!-- Javascripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../js/jquery-1.9.1.min.js"><\/script>')</script>
<script src="../../js/bootstrap.min.js"></script>
<script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
<script src="../../js/jquery.fitvids.js"></script>
<script src="../../js/jquery.sequence-min.js"></script>
<script src="../../js/jquery.bxslider.js"></script>
<script src="../../js/main-menu.js"></script>
<script src="../../js/template.js"></script>

</body>
</html>