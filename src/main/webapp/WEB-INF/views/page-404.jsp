<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Bon Appetit</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

		<link rel="stylesheet" href="../../css/bootstrap.min.css">
		<link rel="stylesheet" href="../../css/icomoon-social.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" href="../../css/leaflet.css" />
		<!--[if lte IE 8]>
		<link rel="stylesheet" href="../../css/leaflet.ie.css" />
		<![endif]-->
		<link rel="stylesheet" href="../../css/main-orange.css">

		<script src="../../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	</head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->


		<!-- Navigation & Logo-->
		<%@ include file="Navigation.jsp" %>

        <!-- Page Title -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1><spring:message code="lang.pageNotFound" text="default"/></h1>
					</div>
				</div>
			</div>
		</div>
        
        <div class="section">
	    	<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="error-page-wrapper">
							<p><spring:message code="lang.pageDosntExist" text="default"/></p>
							<p><spring:message code="lang.tryGoHome" text="default"/> <a href="home"><spring:message code="lang.home" text="default"/></a>?</p>
						</div>
					</div>
				</div>
			</div>
		</div>

	    <!-- Footer -->
		<%@ include file="footer.jsp" %>

        <!-- Javascripts -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../js/jquery-1.9.1.min.js"><\/script>')</script>
		<script src="../../js/bootstrap.min.js"></script>
		<script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
		<script src="../../js/jquery.fitvids.js"></script>
		<script src="../../js/jquery.sequence-min.js"></script>
		<script src="../../js/jquery.bxslider.js"></script>
		<script src="../../js/main-menu.js"></script>
		<script src="../../js/template.js"></script>

    </body>
</html>