<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/icomoon-social.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../../css/leaflet.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="../../css/leaflet.ie.css"/>
    <![endif]-->
    <link rel="stylesheet" href="../../css/main-orange.css">

    <script src="../../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->


<!-- Navigation & Logo-->
<%@ include file="Navigation.jsp" %>
<!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><spring:message code="lang.menu" text="default"/></h1>
                <table class="table">
                    <tr>
                        <td><h2><spring:message code="lang.sortedBy" text="default"/>:</h2></td>
                        <td><a href="${url}?page=0&filter=GARNISH"><spring:message code="lang.garnish" text="default"/></a></td>
                        <td><a href="${url}?page=0&filter=MEAT"><spring:message code="lang.meatDishes" text="default"/></a></td>
                        <td><a href="${url}?page=0&filter=FRIED"><spring:message code="lang.friedDishes" text="default"/></a></td>
                        <td><a href="${url}?page=0&filter=SALAD"><spring:message code="lang.salads" text="default"/></a></td>
                        <td><a href="${url}?page=0&filter=DESSERT"><spring:message code="lang.desserts" text="default"/></a></td>
                        <td><a href="${url}?page=0&filter=DRINK"><spring:message code="lang.drinks" text="default"/></a></td>
                        <td><a href="${url}?page=0&filter="><spring:message code="lang.allDish" text="default"/></a></td>
                    </tr>
                    <tr>
                        <td><a href="${url}?page=0&filter=${filter}&sort=name"><spring:message code="lang.name" text="default"/></a></td>
                    </tr>
                    <tr>
                        <td><a href="${url}?page=0&filter=${filter}&sort=price"><spring:message code="lang.price" text="default"/></a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</div>

<div class="eshop-section section">
    <div class="container">
        <div class="row">
            <c:forEach items="${listOfDishes}" var="dishes">
                <div class="col-md-3 col-sm-6">
                    <!-- Product -->
                    <div class="shop-item">
                        <!-- Product Image -->
                        <c:choose>
                            <c:when test="${dishes.category=='GARNISH'}">
                                <div >
                                    <img src="img/garnish.jpg" alt="Item Name">
                                </div>
                            </c:when>
                            <c:when test="${dishes.category=='MEAT'}">
                                <div class="shop-item-image">
                                    <img src="img/meat.jpg" alt="Item Name">
                                </div>
                            </c:when>
                            <c:when test="${dishes.category=='FRIED'}">
                                <div class="shop-item-image">
                                    <img src="img/freed.jpg" alt="Item Name">
                                </div>
                            </c:when>
                            <c:when test="${dishes.category=='SALAD'}">
                                <div class="shop-item-image">
                                    <img src="img/salad.jpg" alt="Item Name">
                                </div>
                            </c:when>
                            <c:when test="${dishes.category=='DESSERT'}">
                                <div class="shop-item-image">
                                    <img src="img/desert.jpg" alt="Item Name">
                                </div>
                            </c:when>
                            <c:when test="${dishes.category=='DRINK'}">
                                <div class="shop-item-image">
                                    <img src="img/drinks.jpg" alt="Item Name">
                                </div>
                            </c:when>

                        </c:choose>

                        <!-- Product Title -->
                        <div class="price">
                            <h4><a href="/${dishes.id}">${dishes.name}</a></h4>
                        </div>
                        <div class="h5">
                            <b> <spring:message code="lang.category" text="default"/>:</b> ${dishes.category}
                        </div>

                        <div class="h4">
                            <b> <spring:message code="lang.price" text="default"/>:</b> ${dishes.price}
                        </div>
                        <!-- Add to Cart Button -->
                        <c:if test="${pageContext.request.isUserInRole('USER')}">
                            <form method="post" action="addDishToCart">

                                <div class="actions">
                                    <input type="hidden" value="${dishes.id}" name="id"/>
                                    <button class="btn btn-small" type="submit"><i
                                            class="icon-shopping-cart icon-white"></i> <spring:message code="lang.btn.addToCart" text="default"/>
                                    </button>
                                </div>

                            </form>
                        </c:if>
                        <c:if test="${pageContext.request.isUserInRole('ROLE_ADMIN')}">
                            <td class="actions">
                                <a href="add-dish?id=${dishes.id}" class="btn btn-xs btn-grey"><i
                                        class="glyphicon glyphicon-pencil"></i></a>
                                <a href="delete?id=${dishes.id}" class="btn btn-xs btn-grey"><i
                                        class="glyphicon glyphicon-trash"></i></a>
                            </td>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
            <div class="row">
            </div>
            <div class="pagination-wrapper ">
                <ul class="pagination pagination-lg">
                    <c:choose>
                        <c:when test="${page.pageNumber==0}">
                            <li class="disabled"><a href="#"><spring:message code="lang.previous" text="default"/></a></li>
                        </c:when>
                        <c:otherwise>
                            <li class="enable"><a href="${url}?page=${page.pageNumber-1}&filter=${filter}&sort=${sort}"><spring:message code="lang.previous" text="default"/></a></li>
                        </c:otherwise>
                    </c:choose>
                    <c:forEach begin="0" end="${total_count_pages}" var="p">
                        <li class="active"><a href="${url}?page=${p}&filter=${filter}&sort=${sort}">${p}</a></li>
                    </c:forEach>
                    <c:choose>
                        <c:when test="${page.pageNumber==total_count_pages}">
                            <li class="disabled"><a href="#"><spring:message code="lang.next" text="default"/></a></li>
                        </c:when>
                        <c:otherwise>
                            <li class="enable"><a href="${url}?page=${page.pageNumber + 1}&filter=${filter}&sort=${sort}"><spring:message code="lang.next" text="default"/></a></li>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>
        </div>
    </div>


    <!-- Footer -->
    <%@ include file="footer.jsp" %>
    <!-- Javascripts -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../js/jquery-1.9.1.min.js"><\/script>')</script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
    <script src="../../js/jquery.fitvids.js"></script>
    <script src="../../js/jquery.sequence-min.js"></script>
    <script src="../../js/jquery.bxslider.js"></script>
    <script src="../../js/main-menu.js"></script>
    <script src="../../js/template.js"></script>

</body>
</html>