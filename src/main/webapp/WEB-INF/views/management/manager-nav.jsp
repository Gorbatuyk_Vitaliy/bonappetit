
<!-- Navigation & Logo-->
<div class="mainmenu-wrapper">
    <div class="container">

        <nav id="mainmenu" class="mainmenu">
            <ul>
                <li class="active">
                    <a href="showAllOrders">Orders</a>
                </li>
                <li>
                    <a href="viewAccounts">View accounts</a>
                </li>
                <li>
                    <a href="add-dish">Add dish</a>
                </li>
            </ul>
        </nav>
    </div>
</div>
