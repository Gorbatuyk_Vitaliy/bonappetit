<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/icomoon-social.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="css/leaflet.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/leaflet.ie.css"/>
    <![endif]-->
    <link rel="stylesheet" href="css/main-orange.css">

    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->


<!-- Navigation & Logo-->
<%@ include file="../Navigation.jsp" %>

<!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Managment</h1>
                <%@ include file="manager-nav.jsp" %>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <H4>Add new dish</H4>
                </ul>
                <div class="contact-form-wrapper">
                    <form:form method="POST" class="form-horizontal" modelAttribute="newDish" name="saveDish" action="saveDish" >
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label"><b>Dish name</b></label>
                                <div class="col-sm-9">
                                    <spring:bind path="name">
                                    <form:input class="form-control" path="name" name="name" id="name" type="text"></form:input></spring:bind>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label"><b>Description</b></label>
                                <div class="col-sm-9">
                                    <spring:bind path="description">
                                    <form:input class="form-control" path="description" id="description" name="description"
                                                type="text"></form:input></spring:bind>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="price" class="col-sm-3 control-label"><b>Price</b></label>
                                <div class="col-sm-9">
                                    <spring:bind path="price">
                                    <form:input class="form-control" path="price" id="price" name="price" type="text"></form:input></spring:bind>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="cookTime" class="col-sm-3 control-label"><b>Cook time</b></label>
                                <div class="col-sm-9">
                                    <spring:bind path="cookTime">
                                    <form:input class="form-control" path="cookTime" id="cookTime" name="cookTime"
                                                type="time"></form:input></spring:bind>
                                </div>
                            </div>

                        <div class="form-group">
                            <label for="dishOfDay" class="col-sm-3 control-label"><b>Is the dish of day?</b></label>
                            <div class="col-sm-9">
                                <spring:bind path="dishOfDay">
                                    <form:checkbox class="checkbox" path="dishOfDay" id="dishOfDay" name="dishOfDay"></form:checkbox></spring:bind>
                            </div>
                        </div>

                            <div class="form-group">
                                <label for="category" class="col-sm-3 control-label"><b>Category</b></label>
                                <div class="col-sm-9">
                                    <spring:bind path="category">
                                    <form:select class="form-control" path="category" id="category" name="category">
                                        <form:option value="GARNISH">GARNISH</form:option>
                                        <form:option value="MEAT">MEAT</form:option>
                                        <form:option value="FRIED">FRIED</form:option>
                                        <form:option value="SALAD">SALAD</form:option>
                                        <form:option value="DESSERT">DESSERT</form:option>
                                        <form:option value="DRINK">DRINK</form:option>
                                    </form:select></spring:bind>
                                </div>
                            </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn pull-right">Save</button>
                            </div>
                        </div>
                    </form:form>


                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="../footer.jsp" %>
<!-- Javascripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script>
<script src="js/bootstrap.min.js"></script>
<script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery.sequence-min.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/main-menu.js"></script>
<script src="js/template.js"></script>
<script src="js/multipart.js"></script>

</body>
</html>