<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="../../css/bootstrap.min.css">
        <link rel="stylesheet" href="../../css/icomoon-social.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="../../css/leaflet.css" />
		<!--[if lte IE 8]>
		    <link rel="stylesheet" href="../../css/leaflet.ie.css" />
		<![endif]-->
		<link rel="stylesheet" href="../../css/main-orange.css">

        <script src="../../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>

		<%@ include file="Navigation.jsp" %>
		<%@ include file="homepage-slider.jsp" %>

		<!-- Random dish -->
        <div class="section">
	        <div class="container">
				<h2><spring:message code="lang.dishOfDay" text="default"/></h2>
	        	<div class="row">
					<c:forEach var="dish" items="${dishOfDay}">
	        		<div class="col-md-4 col-sm-6">
	        			<div class="shop-item">
                            <c:choose>
                                <c:when test="${dish.category=='GARNISH'}">
                                    <div class="shop-item-image">
                                        <img src="img/garnish.jpg" alt="Item Name">
                                    </div>
                                </c:when>
                                <c:when test="${dish.category=='MEAT'}">
                                    <div class="shop-item-image">
                                        <img src="img/meat.jpg" alt="Item Name">
                                    </div>
                                </c:when>
                                <c:when test="${dish.category=='FRIED'}">
                                    <div class="shop-item-image">
                                        <img src="img/freed.jpg" alt="Item Name">
                                    </div>
                                </c:when>
                                <c:when test="${dish.category=='SALAD'}">
                                    <div class="shop-item-image">
                                        <img src="img/salad.jpg" alt="Item Name">
                                    </div>
                                </c:when>
                                <c:when test="${dish.category=='DESSERT'}">
                                    <div class="shop-item-image">
                                        <img src="img/desert.jpg" alt="Item Name">
                                    </div>
                                </c:when>
                                <c:when test="${dish.category=='DRINKS'}">
                                    <div class="shop-item-image">
                                        <img src="img/drinks.jpg" alt="Item Name">
                                    </div>
                                </c:when>

                            </c:choose>
		        			<h3>${dish.name}</h3>
		        			<p> ${dish.description}</p>

		        			<a href="${dish.id}" class="btn"><spring:message code="lang.btn.readMore" text="default"/></a>
		        		</div>
	        		</div>
					</c:forEach>
	        	</div>
	        </div>
	    </div>
	    <!-- End Random dish -->

		<!-- Call to Action Bar -->
	    <div class="section section-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="calltoaction-wrapper">
							<h3> <spring:message code="lang.text.downloadMenu" text="default"/> </h3> <a href="/pdf/menu.pdf" class="btn btn-orange"><spring:message code="lang.btn.downloadMenu" text="default"/></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Call to Action Bar -->

		<!-- Testimonials -->
	    <div class="section">
			<div class="container">
				<h2><spring:message code="lang.testimonials" text="default"/></h2>
				<div class="row">
					<!-- Testimonial -->
					<div class="testimonial col-md-4 col-sm-6">
						<!-- Author Photo -->
						<div class="author-photo">
							<img src="../../img/user1.jpg" alt="Author 1">
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<!-- Quote -->
								<p class="quote">
		                            "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut."
                        		</p>
                        		<!-- Author Info -->
                        		<cite class="author-info">
                        			- Name Surname,<br>Managing Director at <a href="#">Some Company</a>
                        		</cite>
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
                    <!-- End Testimonial -->
                    <div class="testimonial col-md-4 col-sm-6">
						<div class="author-photo">
							<img src="../../img/user5.jpg" alt="Author 2">
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<p class="quote">
		                            "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo."
                        		</p>
                        		<cite class="author-info">
                        			- Name Surname,<br>Managing Director at <a href="#">Some Company</a>
                        		</cite>
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
					<div class="testimonial col-md-4 col-sm-6">
						<div class="author-photo">
							<img src="../../img/user2.jpg" alt="Author 3">
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<p class="quote">
		                            "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."
                        		</p>
                        		<cite class="author-info">
                        			- Name Surname,<br>Managing Director at <a href="#">Some Company</a>
                        		</cite>
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
				</div>
			</div>
	    </div>
	    <!-- End Testimonials -->

		

		<!-- Our Clients -->
	    <div class="section">
	    	<div class="container">
	    		<h2><spring:message code="lang.ourClients" text="default"/></h2>
				<div class="clients-logo-wrapper text-center row">
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/canon.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/cisco.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/dell.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/ea.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/ebay.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/facebook.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/google.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/hp.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/microsoft.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/mysql.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/sony.png" alt="Client Name"></a></div>
					<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="../../img/logos/yahoo.png" alt="Client Name"></a></div>
				</div>
			</div>
	    </div>
	    <!-- End Our Clients -->

	    <!-- Footer -->
		<%@ include file="footer.jsp" %>

        <!-- Javascripts -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../js/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="../../js/bootstrap.min.js"></script>
        <script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
        <script src="../../js/jquery.fitvids.js"></script>
        <script src="../../js/jquery.sequence-min.js"></script>
        <script src="../../js/jquery.bxslider.js"></script>
        <script src="../../js/main-menu.js"></script>
        <script src="../../js/template.js"></script>

    </body>
</html>