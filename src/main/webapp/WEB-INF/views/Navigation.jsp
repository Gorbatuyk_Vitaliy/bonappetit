<%--
  Created by IntelliJ IDEA.
  User: Vitalik
  Date: 18.05.2021
  Time: 18:44
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Navigation & Logo-->
<div class="mainmenu-wrapper">
    <div class="container">
        <div class="menuextras">
            <div class="extras">
                <ul>
                    <c:if test="${pageContext.request.isUserInRole('USER')}">
                        <li class="shopping-cart-items"><i class="glyphicon glyphicon-shopping-cart icon-white"></i>
                            <a href="showCart"><b>${user.userCart.cartItems.size()} <spring:message code="lang.items"
                                                                                                    text="default"/></b></a>
                        </li>
                    </c:if>
                    <li>
                        <div class="dropdown choose-country">
                            <a class="#" data-toggle="dropdown" href="#"><img src="../../img/flags/gb.png"
                                                                              alt="Great Britain">UK</a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="menuitem"><a href="?lang=ua"><img src="../../img/flags/ua.png" alt="Ukraine">UA</a>
                                </li>
                                <li role="menuitem"><a href="?lang=uk"><img src="../../img/flags/gb.png"
                                                                            alt="Great Britain"/>UK</a></li>

                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="h3"><spring:message code="lang.welcome" text="default"/></div>
                    </li>
                    <li>

                        <div class="dropdown choose-login">
                            <a class="#" data-toggle="dropdown" href="#">
                                <a class="#" data-toggle="dropdown" href="#">
                                    <c:choose>
                                        <c:when test="${pageContext.request.isUserInRole('ROLE_ADMIN')}">

                                            <div class="h3">${user.email}</div>

                                            <ul class="dropdown-menu" role="login">
                                                <li role="loginitem"><a href="<c:url value='/logout' />">Logout</a>
                                                </li>
                                            </ul>
                                        </c:when>
                                        <c:when test="${pageContext.request.isUserInRole('ROLE_USER')}">
                                            <div class="h3">
                                                    ${user.email}
                                            </div>
                                            <ul class="dropdown-menu" role="login">
                                                <li role="loginitem"><a href="showUserProfile"><spring:message
                                                        code="lang.profile" text="default"/></a></li>
                                                <div role="loginitem"><a href="showCart"><spring:message
                                                        code="lang.myCart" text="default"/></a></div>
                                                <li role="loginitem"><a href="<c:url value='/logout' />"><spring:message
                                                        code="lang.logout" text="default"/></a></li>
                                            </ul>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="h1"><a class="h3" href="login"><spring:message code="lang.login"
                                                                                                       text="default"/></a>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </a>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <nav id="mainmenu" class="mainmenu">
            <ul>
                <li class="logo-wrapper"><a href="home"><img src="../../img/bon-appetit-logo.jpg"
                                                             alt="Multipurpose Twitter Bootstrap Template"></a>
                </li>
                <li class="active">
                    <a href="home"><spring:message code="lang.home" text="default"/></a>
                </li>
                <li>
                    <a href="menu"><spring:message code="lang.menu" text="default"/></a>
                </li>
                <li>
                    <a href="about"><spring:message code="lang.about_us" text="default"/></a>
                </li>
                <li>
                    <c:if test="${pageContext.request.isUserInRole('ADMIN')}">
                        <a href="manager"><spring:message code="lang.management" text="default"/></a></c:if>
                </li>
            </ul>
        </nav>
    </div>
</div>
