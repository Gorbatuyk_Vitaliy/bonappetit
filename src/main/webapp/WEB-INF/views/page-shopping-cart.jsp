<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Bon Appetit</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/icomoon-social.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../../css/leaflet.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="../../css/leaflet.ie.css"/>
    <![endif]-->
    <link rel="stylesheet" href="../../css/main-orange.css">

    <script src="../../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>

<!-- Navigation & Logo-->
<%@ include file="Navigation.jsp" %>
<!-- Page Title -->
<div class="section section-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><spring:message code="lang.shoppingCart" text="default"/></h1>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Action Buttons -->
                <div class="pull-right">
                    <div class="h4"><spring:message code="lang.yourMoney" text="default"/>: <b>$${user.cash}</b></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- Shopping Cart Items -->
               <c:if test="${!message.equals('')}"> <div class="error-page-wrapper"><spring:message code="lang.error.cartEmpty" text="default"/></div></c:if>
                <c:forEach var="dishes" items="${userCart}">
                    <form method="post" action="updateCountOfDish">
                        <input type="hidden" name="idDishInCart" value="${userCart.indexOf(dishes)}"/>
                        <table class="shopping-cart">
                            <!-- Shopping Cart Item -->
                            <tr>
                                <!-- Shopping Cart Item Image -->
                                <td class="image"><a href="page-product-details.jsp"><img src="img/dish1.jpg"
                                                                                          alt="Item Name"></a></td>
                                <!-- Shopping Cart Item Description & Features -->
                                <td>
                                    <div class="cart-item-title"><a href="page-product-details.jsp">
                                        <h3>${dishes.dish.name}</h3></a></div>
                                    <div class="feature color">
                                        <div class="h5"><spring:message code="lang.description" text="default"/>: ${dishes.dish.description}</div>
                                    </div>
                                    <div class="feature">
                                        <div class="h5"><spring:message code="lang.category" text="default"/>: <b>${dishes.dish.category}</b></div>
                                    </div>
                                </td>
                                <!-- Shopping Cart Item Quantity -->

                                <td class="quantity">
                                    <input class="form-control input-sm input-micro" type="text" name="countOfDish"
                                           value="${dishes.countOfDish}"/>
                                </td>
                                <!-- Shopping Cart Item Price -->
                                <td class="price">$${dishes.dish.price}</td>
                                <!-- Shopping Cart Item Actions -->
                                <td class="actions">
                                    <button class="btn btn-xs btn-grey"><i class="glyphicon glyphicon-pencil"><spring:message code="lang.update" text="default"/></i>
                                    </button>
                                    <a href="deleteDishFromCart?id=${userCart.indexOf(dishes)}"
                                       class="btn btn-xs btn-grey"><i
                                            class="glyphicon glyphicon-trash"><spring:message code="lang.delete" text="default"/></i></a>
                                </td>
                            </tr>
                            <!-- End Shopping Cart Item -->
                        </table>
                    </form>
                </c:forEach>
                <!-- End Shopping Cart Items -->
            </div>
        </div>
        <form:form modelAttribute="addressInfo" method="post" action="setOrder">
            <div class="row">
                <!-- Promotion Code -->
                <div class="col-md-4  col-md-offset-0 col-sm-6 col-sm-offset-6">
                    <div class="cart-promo-code">
                        <h5><i class="glyphicon glyphicon-inbox"></i> <spring:message code="lang.writeDelAddress" text="default"/>: </h5>
                        <div class="input-group">
                            <spring:bind path="deliveryAddress">
                                <form:input class="form-control input-sm" path="deliveryAddress"
                                            id="appendedInputButton" type="text"
                                            value="" required="required"/>
                            </spring:bind>
                        </div>

                    </div>
                </div>
                <!-- Shipment Options -->
                <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-6">
                    <div class="cart-shippment-options">
                        <h5><i class="glyphicon glyphicon-phone"></i><spring:message code="lang.enterPhone" text="default"/>: </h5>
                        <div class="input-group">
                            <spring:bind path="phoneNumber">
                                <form:input class="form-control input-sm" path="phoneNumber" id="appendedInputButton1"
                                            placeholder="+380__________" type="text" required="required"/>
                            </spring:bind>
                            <div class="alert-danger"><form:errors path="phoneNumber" cssClass="error"/></div>
                        </div>
                    </div>
                </div>

                <!-- Shopping Cart Totals -->

                <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-6">
                    <table class="cart-totals">
                        <tr>
                            <td><b><spring:message code="lang.shipping" text="default"/></b></td>
                            <td><spring:message code="lang.free" text="default"/></td>
                        </tr>
                        <tr class="cart-grand-total">
                            <td><b><spring:message code="lang.totalPrice" text="default"/></b></td>
                            <td><b>$${totalPrice}</b></td>
                        </tr>
                    </table>
                    <!-- Action Buttons -->
                    <div class="pull-right">
                        <button class="btn"><i class="glyphicon glyphicon-shopping-cart icon-white"></i> <spring:message code="lang.order" text="default"/></button>
                    </div>
                </div>
            </div>
        </form:form>
    </div>
</div>
</div>

<!-- Footer -->
<%@ include file="footer.jsp" %>

<!-- Javascripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../js/jquery-1.9.1.min.js"><\/script>')</script>
<script src="../../js/bootstrap.min.js"></script>
<script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js"></script>
<script src="../../js/jquery.fitvids.js"></script>
<script src="../../js/jquery.sequence-min.js"></script>
<script src="../../js/jquery.bxslider.js"></script>
<script src="../../js/main-menu.js"></script>
<script src="../../js/template.js"></script>

</body>
</html>