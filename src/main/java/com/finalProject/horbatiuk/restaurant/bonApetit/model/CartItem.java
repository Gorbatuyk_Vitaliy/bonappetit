package com.finalProject.horbatiuk.restaurant.bonApetit.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.List;


@Entity(name = "cart_item")
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class CartItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
      //          , columnDefinition = "int default '1'"
    @Column(name = "count_of_dish")
    @ColumnDefault(value = "1")
    private int countOfDish;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_id", referencedColumnName = "id")
    private Cart cart;

    @JsonManagedReference
    @ManyToOne()
    @JoinColumn(name = "dish_id", referencedColumnName = "id")
    private Dish dish;

}
