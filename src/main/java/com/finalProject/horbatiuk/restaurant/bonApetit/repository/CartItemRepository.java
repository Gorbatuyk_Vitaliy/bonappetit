package com.finalProject.horbatiuk.restaurant.bonApetit.repository;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.Cart;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.CartItem;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.Dish;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CartItemRepository extends CrudRepository<CartItem, Integer> {

List<CartItem> findCartItemByCart(Cart cart);

}
