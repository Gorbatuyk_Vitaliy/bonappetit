package com.finalProject.horbatiuk.restaurant.bonApetit.repository;


import com.finalProject.horbatiuk.restaurant.bonApetit.model.Cart;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.User;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.management.relation.RoleList;
import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long>, JpaRepository<User, Long> {
    User findUserById(long id);
    User findByName(String name);
    User findByEmail(String email);
    List<User> findAll();



}
