package com.finalProject.horbatiuk.restaurant.bonApetit.controller;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.Dish;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.User;
import com.finalProject.horbatiuk.restaurant.bonApetit.service.DishService;
import com.finalProject.horbatiuk.restaurant.bonApetit.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
public class ListOfDishController {
    private final DishService dishService;
    private final UserService userService;

    public ListOfDishController(DishService dishService, UserService userService) {
        this.dishService = dishService;
        this.userService = userService;
    }

    @GetMapping(value = "/menu")
    public String menuProducts(@RequestParam(required = false, defaultValue = "") String filter,
                               @PageableDefault(value = 8, sort = "name", direction = Sort.Direction.ASC) Pageable pageable,
                               Model model, Principal principal) {

        if (principal != null) {
            User user = userService.findUserByName(principal.getName());
            model.addAttribute("user", user);
        }

        String sort = getLabelOfSort(pageable);

        Page<Dish> dishes;
        if (filter != null && filter.isEmpty()) {
            dishes = dishService.findAll(pageable);
        } else {
            dishes = dishService.findByCategory(filter, pageable);
        }
        model.addAttribute("listOfDishes", dishes.iterator());
        model.addAttribute("url", "/menu");
        model.addAttribute("filter", filter);
        model.addAttribute("sort", sort);
        model.addAttribute("page", pageable);
        model.addAttribute("total_count_pages", dishes.getTotalPages() - 1);
        return "page-products";
    }
    private String getLabelOfSort(Pageable pageable) {
        String sort = pageable.getSort().toString();
        String[] arrSortStr = sort.split(":");
        return arrSortStr[0].trim();
    }
}
