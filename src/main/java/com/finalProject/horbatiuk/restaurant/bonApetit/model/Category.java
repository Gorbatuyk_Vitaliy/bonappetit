package com.finalProject.horbatiuk.restaurant.bonApetit.model;

public enum Category {
    GARNISH, MEAT, FRIED, SALAD, DESSERT, DRINK
}
