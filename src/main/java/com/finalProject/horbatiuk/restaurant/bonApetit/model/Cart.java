package com.finalProject.horbatiuk.restaurant.bonApetit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity(name = "cart")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cart {
    @Id
    @Column(name = "id")
    private long id;


    @Column(name = "data_of_order")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfOrder;
    @Column(name = "total_price")
    private double totalPrice;



    @JsonBackReference
    @OneToOne(mappedBy = "userCart")
    private User user;
    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL)
    private DeliveryAddress deliveryAddress;

    @OneToMany(mappedBy = "cart")
    private List<CartItem> cartItems;


}
