package com.finalProject.horbatiuk.restaurant.bonApetit.service;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.*;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.*;
import com.finalProject.horbatiuk.restaurant.bonApetit.security.CustomUserDetails;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j
@AllArgsConstructor
@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final CartRepository cartRepository;
    private final CartItemRepository cartItemRepository;
    private final DeliveryAddressRepository deliveryAddressRepository;
    private final OrderRepository orderRepository;
    @Autowired
    private final PasswordEncoder bCryptPasswordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByName(username);

        if (Objects.isNull(user)) {
            log.error("No user with user name : " + username);
            throw new UsernameNotFoundException("No user with user name : " + username);
        } else {
            List<String> roles = user.getUserRoleList().stream()
                    .map(UserRole::getRole)
                    .collect(Collectors.toList());
            return new CustomUserDetails(roles, user);
        }
    }

    @Transactional
    public void saveNewUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setCash(500);
        List<UserRole> userRoleList = new ArrayList<>();
        userRoleList.add(userRoleRepository.getOne(0L));
        user.setUserRoleList(userRoleList);
        userRepository.save(user);
        User userFromDB = userRepository.findByName(user.getName());
        setCartAndDeliveriAddressToUser(userFromDB);
        log.info("Created new user " + user.getName());
    }


    public void setCartAndDeliveriAddressToUser(User user) {
        Cart cart = Cart.builder()
                .id(user.getId())
                .build();
        DeliveryAddress deliveryAddress = DeliveryAddress.builder()
                .id(user.getId())
                .build();
        cart.setDeliveryAddress(deliveryAddress);
        user.setUserCart(cart);
        deliveryAddressRepository.save(deliveryAddress);
        userRepository.save(user);
        cartRepository.save(cart);
        log.info("set Cart And Delivery Address To User " + user.getName());
    }

    public void updateRoleFromUser(User user, long idRole) {
        List<UserRole> userRoleList = new ArrayList<>();
        userRoleList.add(userRoleRepository.getOne(idRole));
        user.setUserRoleList(userRoleList);
        userRepository.save(user);
        log.info(user.getName() + " change role " + user.getUserRoleList().get(0));
    }


    public void deleteUser(long id) {
        User user = userRepository.findUserById(id);
        userRepository.delete(user);
        log.info("User " + user.getName() + " successful delete");
    }

    public User findUserByName(String name) {
        return userRepository.findByName(name);
    }


    public User findUserById(long id) {
        return userRepository.findUserById(id);
    }

    public List<UserRole> findRoleByUser(User user) {
        return userRoleRepository.findUserRolesById(user.getId());
    }


    public List<User> findAll() {
        return userRepository.findAll();
    }

    public double getTotalPrice(List<CartItem> dishList) {
        double totalPrice = 0;
        for (CartItem cartItem : dishList) {
            totalPrice = totalPrice + cartItem.getDish().getPrice() *
                    cartItem.getCountOfDish();
        }
        BigDecimal totalPriceRound = new BigDecimal(totalPrice);
        MathContext m = new MathContext(5);

        return totalPriceRound.round(m).doubleValue();
    }

    public void updateCountOfDishInCart(int countOfDish, int idDish, List<CartItem> dishList) {
        dishList.get(idDish).setCountOfDish(countOfDish);
        CartItem cartItem = dishList.get(idDish);
        cartItemRepository.save(cartItem);
    }

    public void deleteDishFromCart(User user, int dishIdFromCartItemList) {
        CartItem cartItem = user.getUserCart().getCartItems().get(dishIdFromCartItemList);
        user.getUserCart().getCartItems().remove(dishIdFromCartItemList);
        cartItemRepository.delete(cartItem);
        log.info("Delete dish " + dishIdFromCartItemList + " from cart user" + user.getName());
    }

    public void updateUserCash(Order order) {
        User user = order.getUser();
        double totalPrice = order.getTotalPrice();
        double totalPriceFromUser = user.getCash() - totalPrice;
        user.setCash(totalPriceFromUser);
        userRepository.save(user);
        log.info("Confirm " + user.getName() + "s order and pay");
    }

    public void setUserCash(User user, double money) {
        double currentUserCash = user.getCash();
        user.setCash(money + currentUserCash);
        userRepository.save(user);
        log.info("User "+ user.getName() + "update own cash: " + user.getCash());
    }

    public void checkUserCash(User user) {
        double totalPrice = user.getUserCart().getTotalPrice();
        double totalPriceFromUser = user.getCash() - totalPrice;
        if (totalPriceFromUser < 0) {
            log.error(user.getName() + " doesn't have enough money");
            throw new ArithmeticException("Unfortunately you need to have more money on your balance. You have: "
                    + user.getCash() + " but need " + totalPrice);
        }
    }

    public void saveTotalPriceInCart(double totalPrice, User user) {
        Cart cart = user.getUserCart();
        cart.setTotalPrice(totalPrice);
        cartRepository.save(cart);
    }


    public boolean checkIfEmptyCartItem(List<CartItem> dishList) {
        return dishList.size() == 0;
    }
}
