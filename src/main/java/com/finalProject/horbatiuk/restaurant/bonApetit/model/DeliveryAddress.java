package com.finalProject.horbatiuk.restaurant.bonApetit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryAddress {
    @Id
    private long id;
    @Column(name = "delivery_adress")
    private String deliveryAddress;
    @Column(name = "phone_number")
    private String phoneNumber;
    @JsonBackReference
    @OneToOne(mappedBy = "deliveryAddress")
    private Cart carAddressDelivery;

}
