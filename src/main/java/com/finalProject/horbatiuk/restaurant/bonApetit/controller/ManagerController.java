package com.finalProject.horbatiuk.restaurant.bonApetit.controller;


import com.finalProject.horbatiuk.restaurant.bonApetit.model.*;
import com.finalProject.horbatiuk.restaurant.bonApetit.service.CartService;
import com.finalProject.horbatiuk.restaurant.bonApetit.service.DishService;
import com.finalProject.horbatiuk.restaurant.bonApetit.service.OrderService;
import com.finalProject.horbatiuk.restaurant.bonApetit.service.UserService;
import com.finalProject.horbatiuk.restaurant.bonApetit.validator.UserValidator;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Log4j
@AllArgsConstructor
@Controller
public class ManagerController {
    private final UserService userService;
    private final CartService cartService;
    private final UserValidator userValidator;
    private final DishService dishService;
    private final OrderService orderService;


    @GetMapping("/manager")
    public String managerMenu() {
        return "redirect:/showAllOrders";
    }

    @GetMapping("/add-dish")
    public String formAddDish(Model model,
                              @RequestParam(defaultValue = "", required = false) Integer id,
                              Principal principal) {
        if (principal != null) {
            User user = userService.findUserByName(principal.getName());
            model.addAttribute("user", user);
        }
        if (id != null) {
            model.addAttribute("newDish", dishService.findDishById(id));
        } else {
            model.addAttribute("newDish", new Dish());
        }

        return "management/add-dish";
    }


    @GetMapping("/delete")
    public String deleteDish(@RequestParam("id") int id) {
        dishService.deleteDish(id);
        return "redirect:menu";
    }

    @PostMapping("/saveDish")
    public String saveDish(
            @ModelAttribute("newDish") Dish dish,
            Model model) {
        dishService.saveDish(dish);
        log.info("Dish " + dish.getName() + " successful add to DB");
        return "management/add-dish";
    }

    @GetMapping("/viewAccounts")
    public String showAccounts(Model model, Principal principal) {
        if (principal != null) {
            User user = userService.findUserByName(principal.getName());
            model.addAttribute("user", user);
        }
        List<User> userList = userService.findAll();
        model.addAttribute("users", userList);
        return "management/accounts";
    }

    @PostMapping("/editUserRole")
    public String editUserRole(@RequestParam("idUser") int idUser,
                               @RequestParam("role") int idRole) {
        User user = userService.findUserById(idUser);
        userService.updateRoleFromUser(user, idRole);
        return "redirect:viewAccounts";
    }

    @GetMapping("/deleteUser")
    public String deleteUser(@RequestParam long id) {
        userService.deleteUser(id);
        return "redirect:viewAccounts";
    }

    @GetMapping("/showAllOrders")
    public String showAllOrders(@RequestParam(required = false, defaultValue = "") String filter,
                                @RequestParam(required = false, defaultValue = "0", value = "userId") String idStr,
                                Model model, Principal principal) {
        long id = Long.parseLong(idStr);
        List<Order> orders;
        User userOrder = userService.findUserById(id);
        if (filter.equals("viewAll")) {
            orders = orderService.findAllOrders();
        } else {
            if (id > 0) {
                orders = orderService.findOrderByUserSort(userOrder, OrderStatus.COOKING);
            } else {
                orders = orderService.findAllOrders(OrderStatus.COOKING);
            }
        }

        if (principal != null) {
            User user = userService.findUserByName(principal.getName());
            model.addAttribute("user", user);
        }
        OrderStatus orderStatusDone = OrderStatus.DONE;
        model.addAttribute("statusDone", orderStatusDone);
        model.addAttribute("url", "/showAllOrders");
        model.addAttribute("filter", filter);
        model.addAttribute("orders", orders);
        return "management/page-orders-manager";
    }

    @PostMapping("/changeOrders")
    public String ChangeStatusOrder(@RequestParam("orderStatus") OrderStatus orderStatus,
                                    @RequestParam("idOrder") long id) {
        orderService.setNewStatus(id, orderStatus);
        orderService.updateCash(id, orderStatus);
        return "redirect:/showAllOrders";
    }
}
