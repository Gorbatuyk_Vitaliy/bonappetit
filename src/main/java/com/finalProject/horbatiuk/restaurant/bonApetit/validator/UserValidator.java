package com.finalProject.horbatiuk.restaurant.bonApetit.validator;


import com.finalProject.horbatiuk.restaurant.bonApetit.model.User;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class UserValidator implements Validator {

    private final UserRepository userService;

    @Autowired
    public UserValidator(UserRepository userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        User user = (User) o;
        // ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");

        if ( userService.findByName(user.getName())!= null) {
            errors.rejectValue("name", "duplicate.userForm.userEmail");
        }

        if (!user.getConfirmPassword().equals(user.getPassword())) {
            errors.rejectValue("confirmPassword", "different.userForm.password");
        }
        if (!user.getName().contains("@")) {
            errors.rejectValue("name", "email.userForm.contain");
        }
    }
}
