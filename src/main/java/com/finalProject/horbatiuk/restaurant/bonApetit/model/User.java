package com.finalProject.horbatiuk.restaurant.bonApetit.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NotEmpty(message = "Name must not be empty")
    @Column(unique = true)
    @NotEmpty(message = "This field must not be empty")
    private String name;

    @NotEmpty(message = "Password must not be empty")
    @Size(min = 4, message = "Password must more then 4 characters")
    private String password;
    @Transient
    private String confirmPassword;

    private String email;
    @ColumnDefault(value = "500")
    private double cash;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user.id"),
            inverseJoinColumns = @JoinColumn(name = "user_role.id"))
    private List<UserRole> userRoleList;
    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL)
    private Cart userCart;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Order> orders;


}
