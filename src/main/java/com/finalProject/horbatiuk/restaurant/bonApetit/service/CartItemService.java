package com.finalProject.horbatiuk.restaurant.bonApetit.service;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.Cart;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.CartItem;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.User;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.CartItemRepository;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Log4j
@AllArgsConstructor
@Service
public class CartItemService {
    private final CartItemRepository cartItemRepository;

    public void deleteCarItem(User user) {
        Cart cart = user.getUserCart();
        List<CartItem> cartItemList = cartItemRepository.findCartItemByCart(cart);
        if (cartItemList.size() > 0) {
            for (CartItem cartItem : cartItemList) {
                cartItemRepository.delete(cartItem);
            }
        }else {
            log.error("Failure to remove cart item - cart item is empty");
            throw new NullPointerException("You cart is empty!");
        }
    }



}
