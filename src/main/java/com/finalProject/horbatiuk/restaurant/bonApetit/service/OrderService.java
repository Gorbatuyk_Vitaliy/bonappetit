package com.finalProject.horbatiuk.restaurant.bonApetit.service;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.*;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.DishRepository;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.OrderRepository;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.hibernate.dialect.DataDirectOracle9Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final DishRepository dishRepository;
    private final UserService userService;

    public void saveOrder(Order order) {
        orderRepository.save(order);
    }

    public void saveOrderForUser(User user) {
        List<Dish> dishList = new ArrayList<>();
        List<CartItem> cartItems = user.getUserCart().getCartItems();
        for (CartItem cartItem : cartItems) {
            dishList.add(cartItem.getDish());
        }

        Order order = Order.builder()
                .user(user)
                .deliveryAddress(user.getUserCart().getDeliveryAddress().getDeliveryAddress())
                .phone(user.getUserCart().getDeliveryAddress().getPhoneNumber())
                .dateOfOrder(user.getUserCart().getDateOfOrder())
                .totalPrice(user.getUserCart().getTotalPrice())
                .dishList(dishList)
                .orderStatus(OrderStatus.COOKING)
                .build();
        orderRepository.save(order);

        List<Order> orderList = new ArrayList<>();
        orderList.add(order);
        user.setOrders(orderList);
        userRepository.save(user);
    }

    public List<Order> findOrderByUserSort(User user, OrderStatus orderStatus ) {
        return orderRepository.findOrderByUserAndOrderStatus(user, orderStatus, Sort.by("dateOfOrder").descending());
    }

    public List<Order> findOrderByUser(User user) {
        return orderRepository.findOrderByUser(user, Sort.by("dateOfOrder").descending());
    }

    public List<Order> findAllOrders(OrderStatus orderStatus) {
        return orderRepository.findAllByOrderStatus(orderStatus, Sort.by("dateOfOrder").descending());
    }

    public List<Order> findAllOrders(){
      return orderRepository.findAll();
    }

    public Order findOrderById(long id) {
        return orderRepository.findOrderById(id);
    }

    public void setNewStatus(long id, OrderStatus orderStatus) {
        Order order = findOrderById(id);
        order.setOrderStatus(orderStatus);
        saveOrder(order);
        if (orderStatus.equals(OrderStatus.DONE)){
            userService.updateUserCash(order);
        }
    }

    public void updateCash(long id, OrderStatus orderStatus) {
        Order order = findOrderById(id);
        if (orderStatus.equals(OrderStatus.DONE)){
            userService.updateUserCash(order);
        }
    }
}
