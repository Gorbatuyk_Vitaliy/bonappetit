package com.finalProject.horbatiuk.restaurant.bonApetit.config;


import lombok.AllArgsConstructor;

import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@AllArgsConstructor
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/index").permitAll()
                .antMatchers("/menu").permitAll()
                .antMatchers("/about").permitAll()
                .antMatchers("/testimonials").permitAll()
                .antMatchers("/page-shopping-cart").hasRole("USER")
                .antMatchers("/management/**").hasRole("ADMIN")
                .antMatchers("/add-dish").hasRole("ADMIN")
                .antMatchers("/viewAccounts").hasRole("ADMIN")
                .antMatchers("/showAllOrders").hasRole("ADMIN")
                .antMatchers("/editUserRole").hasRole("ADMIN")
                .antMatchers("/deleteUser").hasRole("ADMIN")
                .antMatchers("/saveDish").hasRole("ADMIN")
                .and()
                .formLogin()
                .loginPage("/login").loginProcessingUrl("/j_spring_security_check")
                .usernameParameter("username").passwordParameter("password")
                .defaultSuccessUrl("/menu")
                .and()
                .logout()
                .logoutUrl("/j_spring_security_logout")
                .logoutSuccessUrl("/login")
                .and().exceptionHandling().accessDeniedPage("/error_info")
                .and().csrf().disable();
    }

    @Bean(name = "passwordEncoder")
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
