package com.finalProject.horbatiuk.restaurant.bonApetit.validator;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.DeliveryAddress;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class ShopCartValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return DeliveryAddress.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        DeliveryAddress deliveryAddress = (DeliveryAddress) o;

        if (!deliveryAddress.getPhoneNumber().matches("^\\+?3?8?(0[5-9][0-9]\\d{7})$")) {
            errors.rejectValue("phoneNumber", "phoneNumber.shopingCart.notMachExample");
        }
    }
}
