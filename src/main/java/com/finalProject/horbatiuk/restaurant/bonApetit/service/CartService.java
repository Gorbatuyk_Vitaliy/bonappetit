package com.finalProject.horbatiuk.restaurant.bonApetit.service;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.*;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.CartItemRepository;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.CartRepository;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.DeliveryAddressRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class CartService {
    private final CartRepository cartRepository;
    private final DeliveryAddressRepository deliveryAddressRepository;
    private final UserService userService;
    private final CartItemRepository cartItemRepository;
    private final DishService dishService;


    @Transactional
    public void addDishToCart(User user, int dishId) {
        Cart cart = user.getUserCart();
        Optional<CartItem> cartItem = cart.getCartItems().stream()
                .filter(dish -> dish.getDish().getId()==dishId).findAny();
        if(cartItem.isPresent()){

            cartItem.ifPresent(this::increaseCount);

        }else {
            Dish dish = dishService.findDishById(dishId);
            CartItem newCartItem = CartItem.builder()
                    .dish(dish)
                    .cart(cart)
                    .countOfDish(1)
                    .build();
            cartItemRepository.save(newCartItem);
            cart.getCartItems().add(newCartItem);
        }

    }
    public void addAddressToCart(User user, String address, String phoneNumber) {
        Cart cart = user.getUserCart();
        DeliveryAddress deliveryAddress = cart.getDeliveryAddress();
        deliveryAddress.setPhoneNumber(phoneNumber);
        deliveryAddress.setDeliveryAddress(address);
        cart.setDeliveryAddress(deliveryAddress);

        deliveryAddressRepository.save(deliveryAddress);
        cartRepository.save(cart);
    }

    private void increaseCount(CartItem cartItem) {
        cartItem.setCountOfDish(cartItem.getCountOfDish() + 1);
        cartItemRepository.save(cartItem);
    }

    public List<CartItem> getListDish(User user){
        Cart cart = user.getUserCart();
        List<CartItem> cartItemList= cart.getCartItems();

        return cart.getCartItems();
    }


    public void addLocalTimeToOrder(User user) {
        Cart cart = user.getUserCart();
        Date date = new Date();
        cart.setDateOfOrder(date);
    }
}

