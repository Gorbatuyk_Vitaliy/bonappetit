package com.finalProject.horbatiuk.restaurant.bonApetit.repository;


import com.finalProject.horbatiuk.restaurant.bonApetit.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole, Long>, JpaRepository<UserRole, Long> {

    List<UserRole> findUserRolesById(long userId);
}
