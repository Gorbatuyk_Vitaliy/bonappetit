package com.finalProject.horbatiuk.restaurant.bonApetit.repository;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.CartItem;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.Dish;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DishRepository extends JpaRepository<Dish, Integer>,
        CrudRepository<Dish, Integer>, PagingAndSortingRepository<Dish, Integer> {

    Dish findDishById(int id);
    List <Dish> findAllByDishOfDay(boolean b);
    Page<Dish> findAllByCategory(String category, Pageable pageable);

    List<Dish> findDishByCartItem(CartItem cartItem);

    Page<Dish> findAll(Pageable pageable);

}