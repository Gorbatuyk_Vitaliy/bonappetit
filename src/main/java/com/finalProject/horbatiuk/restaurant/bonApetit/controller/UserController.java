package com.finalProject.horbatiuk.restaurant.bonApetit.controller;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.*;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.DishRepository;
import com.finalProject.horbatiuk.restaurant.bonApetit.service.*;
import com.finalProject.horbatiuk.restaurant.bonApetit.validator.ShopCartValidator;
import com.finalProject.horbatiuk.restaurant.bonApetit.validator.UserValidator;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.List;

@Log4j
@Controller
public class UserController {
    private final UserService userService;
    private final CartService cartService;
    private final UserValidator userValidator;
    private final CartItemService cartItemService;
    private final OrderService orderService;
    private final ShopCartValidator shopCartValidator;
    private final DishService dishService;
    private final ServletContext context;

    @Autowired
    public UserController(UserService userService, CartService cartService, UserValidator userValidator, DishRepository dishRepository, CartItemService cartItemService, OrderService orderService, ShopCartValidator shopCartValidator, DishService dishService, ServletContext context) {
        this.userService = userService;
        this.cartService = cartService;
        this.userValidator = userValidator;
        this.cartItemService = cartItemService;
        this.orderService = orderService;
        this.shopCartValidator = shopCartValidator;
        this.dishService = dishService;
        this.context = context;
    }



    @GetMapping("/home")
    public String goHome(Principal principal, Model model) {
        if (principal != null) {
            User user = userService.findUserByName(principal.getName());
            model.addAttribute("user", user);
        }
        List<Dish> dishList = dishService.findDishOfDay(true);
        model.addAttribute("dishOfDay", dishList);
        return "index";
    }

    @GetMapping("/about")
    public String goAbout(Principal principal, Model model) {
        if (principal != null) {
            User user = userService.findUserByName(principal.getName());
            model.addAttribute("user", user);
        }
        return "page-about-us";
    }


    @GetMapping(value = "/register")
    public String registration(Model model) {
        model.addAttribute("user", new User());
        return "page-register";
    }


    @PostMapping(value = "/register")
    public String registration(@ModelAttribute("user") @Valid User user,
                               BindingResult bindingResult, Model model) {
        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            return "page-register";
        }
        userService.saveNewUser(user);
        return "redirect:/login";
    }


    @GetMapping("/login")
    public String goLoginForm(Model model) {
        model.addAttribute("user", new User());
        return "page-login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        if (auth != null) {
            log.info("User "+ auth.getName()+ " is logout");
        }
        return "redirect:/login";
    }

    @GetMapping("/{id}")
    public String showProductDetail(@PathVariable("id") int id, Model model, Principal principal) {
        if (principal != null) {
            User user = userService.findUserByName(principal.getName());
            model.addAttribute("user", user);
        }
        Dish dish = dishService.findDishById(id);
        model.addAttribute("dish", dish);
        return "page-product-details";
    }

    @RequestMapping("/pdf/{fileName:.+}")
    public void downloader(HttpServletRequest request, HttpServletResponse response,
                           @PathVariable("fileName") String fileName) {

        String downloadFolder = context.getRealPath("/WEB-INF/");
        Path file = Paths.get(downloadFolder, fileName);
        // Check if file exists
        if (Files.exists(file)) {
            // set content type
            response.setContentType("application/pdf");
            // add response header
            response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
            try {
                // copies all bytes from a file to an output stream
                Files.copy(file, response.getOutputStream());
                // flushes output stream
                response.getOutputStream().flush();

            } catch (IOException e) {
                log.error(e);
                e.printStackTrace();
            }
        } else {
            log.error("Download Menu-file not exist");
        }
    }

    @PostMapping("/updateCountOfDish")
    public String updateCountOfDish(@RequestParam("countOfDish") int countOfDish,
                                    @RequestParam("idDishInCart") int idDishInCart,
                                    Principal principal
    ) {
        User user = userService.findUserByName(principal.getName());
        List<CartItem> dishList = cartService.getListDish(user);
        userService.updateCountOfDishInCart(countOfDish, idDishInCart, dishList);
        return "redirect:/showCart";
    }


    @PostMapping("/addDishToCart")
    public String addDishTCart(@RequestParam("id") int id, Principal principal) {
        User user = userService.findUserByName(principal.getName());
        cartService.addDishToCart(user, id);
        return "redirect:/menu";
    }

    @GetMapping("/deleteDishFromCart")
    public String deleteDishFromCart(@RequestParam("id") int dishIdFromCartItemList, Principal principal) {
        User user = userService.findUserByName(principal.getName());
        userService.deleteDishFromCart(user, dishIdFromCartItemList);
        return "redirect:/showCart";
    }

    @GetMapping("/showCart")
    public String showCart(Model model, Principal principal, @ModelAttribute(value = "massage-error", binding = false) String messageError) {
        User user = userService.findUserByName(principal.getName());
        List<CartItem> dishList = cartService.getListDish(user);

        double totalPrice = userService.getTotalPrice(dishList);

        if (!model.containsAttribute("addressInfo")) {
            model.addAttribute("addressInfo", new DeliveryAddress());
        }
        model.addAttribute("message", messageError);
        model.addAttribute("userCart", dishList);
        model.addAttribute("user", user);
        model.addAttribute("totalPrice", totalPrice);
        return "page-shopping-cart";
    }

    @PostMapping("/setOrder")
    @Transactional
    public String setOrder(Principal principal,
                           @ModelAttribute("addressInfo") @Validated DeliveryAddress addressInfo,
                           BindingResult bindingResult, RedirectAttributes redirectAttributes,
                           Model model) {
        shopCartValidator.validate(addressInfo, bindingResult);
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.addressInfo", bindingResult);
            redirectAttributes.addFlashAttribute("addressInfo", addressInfo);
            return "redirect:/showCart";
        }
        User user = userService.findUserByName(principal.getName());
        cartService.addAddressToCart(user, addressInfo.getDeliveryAddress(),
                addressInfo.getPhoneNumber());
        cartService.addLocalTimeToOrder(user);
        List<CartItem> dishList = cartService.getListDish(user);

        if (userService.checkIfEmptyCartItem(dishList)) {
            log.error("User " + user.getName()+ " cart item is empty");
            redirectAttributes.addFlashAttribute("massage-error", "Your cart is empty!");
            return "redirect:/showCart";
        }
        double totalPrice = userService.getTotalPrice(dishList);
        userService.saveTotalPriceInCart(totalPrice, user);

        userService.checkUserCash(user);
        return "redirect:/viewCardOrder";
    }

    @GetMapping("/viewCardOrder")
    public String viewOrder(Principal principal, Model model) {
        User user = userService.findUserByName(principal.getName());
        model.addAttribute(user);
        return "page-confirm-order";
    }

    @PostMapping("/confirmPay")
    @Transactional
    public String confirmPay(Principal principal) {
        User user = userService.findUserByName(principal.getName());
        orderService.saveOrderForUser(user);
        userService.setCartAndDeliveriAddressToUser(user);
        cartItemService.deleteCarItem(user);
        log.info("User " + user.getName() + " made a successful made order. Order waite for confirm manager");
        return "redirect:/showCart";
    }

    @GetMapping("/returnOnPageCart")
    public String returnToViewCart() {
        return "redirect:/showCart";
    }

    @GetMapping("/showUserProfile")
    public String showUserProfile(Principal principal, Model model) {
        User user = userService.findUserByName(principal.getName());
        List<Order> orders = orderService.findOrderByUser(user);
        model.addAttribute("orders", orders);
        model.addAttribute("user", user);
        return "page-user-profile";
    }

    @PostMapping("/addMoney")
    public String addMoney(@RequestParam("money") double money, Principal principal) {
        User user = userService.findUserByName(principal.getName());
        userService.setUserCash(user, money);
        return "redirect:/showUserProfile";
    }

    @GetMapping("/error_info")
    public String errorPage() {
        return "page-404";
    }
}
