package com.finalProject.horbatiuk.restaurant.bonApetit;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.Dish;
import com.finalProject.horbatiuk.restaurant.bonApetit.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BonApetitApplication {

    public static void main(String[] args) {
        SpringApplication.run(BonApetitApplication.class, args);
    }
}
