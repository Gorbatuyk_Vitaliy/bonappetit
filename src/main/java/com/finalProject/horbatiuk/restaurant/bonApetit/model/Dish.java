package com.finalProject.horbatiuk.restaurant.bonApetit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "dish")
public class Dish {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String description;


    private String category;
    private double price;

    @Column(name = "cook_time")
    private LocalTime cookTime;

    private boolean dishOfDay;

    @JsonBackReference
    @OneToMany(mappedBy = "dish")
    private List<CartItem> cartItem;



    @ManyToMany(mappedBy = "dishList")
    private List<Order> orderList;

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", price=" + price +
                ", cookTime=" + cookTime +
                '}';
    }
}
