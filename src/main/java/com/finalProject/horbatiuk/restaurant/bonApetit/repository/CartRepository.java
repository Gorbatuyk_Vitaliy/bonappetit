package com.finalProject.horbatiuk.restaurant.bonApetit.repository;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.Cart;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.CartItem;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.Dish;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Integer>, CrudRepository<Cart, Integer> {

    Cart findCartById(long id);

    Cart findCartByUser(User user);


}
