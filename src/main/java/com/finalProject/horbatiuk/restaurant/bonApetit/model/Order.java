package com.finalProject.horbatiuk.restaurant.bonApetit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "customer_order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;


    @Column(name = "data_of_order")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfOrder;
    @Column(name = "total_price")
    private double totalPrice;
    private String deliveryAddress;
    private String phone;

    @Enumerated(EnumType.STRING)
    @Column(name = "order_status")
    private OrderStatus orderStatus;

    @JsonBackReference
    @ManyToOne()
    private User user;


    @ManyToMany()
    @JoinTable(name = "dish_in_order", joinColumns = @JoinColumn(name = "dish_id.id"),
            inverseJoinColumns = @JoinColumn(name = "order_id.id"))
    private List<Dish> dishList;

}
