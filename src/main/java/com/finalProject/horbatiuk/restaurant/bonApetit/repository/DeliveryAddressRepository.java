package com.finalProject.horbatiuk.restaurant.bonApetit.repository;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.DeliveryAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface DeliveryAddressRepository extends JpaRepository<DeliveryAddress, Integer>,
        CrudRepository<DeliveryAddress, Integer> {
    DeliveryAddress findDeliveryAddressById(long id);
}
