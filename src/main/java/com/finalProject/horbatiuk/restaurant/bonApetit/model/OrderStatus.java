package com.finalProject.horbatiuk.restaurant.bonApetit.model;

public enum OrderStatus {
    COOKING, READY, WAITE, DEPARTED, DONE
}

