package com.finalProject.horbatiuk.restaurant.bonApetit.repository;

import com.finalProject.horbatiuk.restaurant.bonApetit.model.Order;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.OrderStatus;
import com.finalProject.horbatiuk.restaurant.bonApetit.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long>, PagingAndSortingRepository<Order, Long> {
List<Order> findOrderByUser(User user,Sort sort );
    List<Order> findOrderByUserAndOrderStatus(User user,OrderStatus orderStatus, Sort sort );
//@Query(value = "SELECT id,data_of_order,delivery_address,phone,total_price,order_status from customer_order where order_status = 'COOKING' order by data_of_order desc", nativeQuery = true)
    List<Order> findAllByOrderStatus(OrderStatus orderStatus, Sort sort);
    List<Order> findAll();
    Order findOrderById(long id);
}
