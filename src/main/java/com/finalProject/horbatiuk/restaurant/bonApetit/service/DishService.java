package com.finalProject.horbatiuk.restaurant.bonApetit.service;


import com.finalProject.horbatiuk.restaurant.bonApetit.model.Dish;
import com.finalProject.horbatiuk.restaurant.bonApetit.repository.DishRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.List;


@AllArgsConstructor
@Service
public class DishService {
    private final DishRepository dishRepository;




    public void saveDish(Dish dish) {
        dishRepository.save(dish);
    }
    public List<Dish> findDishOfDay(boolean b){
       return dishRepository.findAllByDishOfDay(b);
    }

    public Dish findDishById(int id) {
        return dishRepository.findDishById(id);
    }

    public void deleteDish(int id) {
        dishRepository.deleteById(id);
    }


    public List<Dish> sortedByName(String name, Pageable pageable) {
        return dishRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    public Page<Dish> findByCategory(String category, Pageable pageable) {
        return dishRepository.findAllByCategory(category, pageable);
    }

    public Page<Dish> findAll(Pageable pageable) {
        return dishRepository.findAll(pageable);

    }


}
