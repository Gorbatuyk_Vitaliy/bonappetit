
use bon_appetit;
insert into user_role (id,role)
values
(0, "ROLE_USER"),
(1, "ROLE_ADMIN");

insert into user (id, cash, name, email)
value (1, 500, "admin@gmail.com", "admin");

update  users_roles set user_role_id=1 where user_id=1;

insert into dish (id, dish_of_day, category, cook_time, description, name, price )
values 
(1, false,"SALAD", "02:02","Quinoa mix with orange slices and spinach, pear slices, Adygea cheese, avocado, dried cranberries, walnuts and cilantro.", "Salad with quinoa, pear and Adygea cheese", 5.0),
(2,false, "SALAD", "02:02","A light salad of fresh tuna in a marinade from the chef with quail eggs and slices of nori-avocado. Edamame beans, iceberg lettuce and fresh cucumber perfectly emphasize the taste of tuna, dried paprika and chili threads add a delicious aroma to the dish.", "Best ever Ahi Tuna salad", 10.0),
(3,false, "SALAD", "02:02","Fueru Wakame and Hiyashi Wakame seaweed with local soy sprouts, seasonal berries and edamame beans, seasoned with homemade sesame sauce.", "Fueru salad with local sprouts Mung", 33.0),
(4,false, "SALAD", "02:02","Quinoa mix with slices of orange, avocado and spinach, topped with pear slices and Tofu cheese, garnished with walnuts, dried cranberries and cilantro.", "Quinoa, pear and tofu salad", 12.0),
(5,false, "SALAD", "02:02","Mixed salad, teriyaki chicken, avocado, feta cheese, grapes, cherry tomatoes, sesame seeds, lemon dressing.", "FETAGRAPE", 21.0),
(6,false, "SALAD", "02:02","Mixed salad, chicken fillet, tomato, croutons, Caesar sauce, chicken egg, Parmesan.", "CAESAR CHICKEN SALAD", 21.0),
(7,false, "SALAD", "02:02","Mixed salad, turkey fillet, cherry tomatoes, pear, avocado, brie cheese, Nasharab sauce.", "TURKEY SALAD", 13.0),
(8,false, "SALAD", "02:02","Mixed salad, roast beef, sun-dried tomatoes, purple onions, baked potatoes, mustard dressing", "VEAL SALAD", 14.5),

(9,false, "DESSERT", "02:02","An unusual honey cake without honey and white sugar, which melts in your mouth and returns to childhood, straight to my grandmother's kitchen.", "Honeycomb without honey", 10.0),
(10,false, "DESSERT", "02:02"," A traditional low-calorie dessert of Japanese cuisine, made from a specia", "Honeycomb without honey", 24.0),
(11,false, "DESSERT", "02:02","Moti is a traditional low-calorie dessert of Japanese cuisine, made from a special rice dough with creamy fruit filling. Moti tastes better cold, so you should try them in the first 40 minutes.", "Moti", 26.3),
(12,false, "DESSERT", "02:02","A light dessert based on tofu and coconut caramel. Stuffing - salted caramel, does not contain white sugar, nuts, lactose. Sweetened only with Jerusalem artichoke syrup and covered with vegan chocolate.", "Tofu glazed with vegan dark chocolate", 19.4),
(13,true, "DESSERT", "02:02","Our iconic sneakers based on vegan peanut nougat with viscous caramel in coconut milk with the addition of crispy peanuts.", "Craft smart sneakers", 14.0),
(14,false, "DESSERT", "02:02","Cream, egg yolks, sugar, orange peel, vanilla sugar", "CREAM BRULE", 10.0),
(15,false, "DESSERT", "02:02","Dark chocolate, avocado, vanilla sugar, sugar, cocoa powder, walnut", "CHOCOLATE TRUFFLES WITH AVOCADO", 12.0),

(16, true, "GARNISH", "02:02","Potatoes baked with rosemary and garlic", "Potatoes with rosemary", 15.6),
(17,false, "GARNISH", "02:02","In a creamy sauce with mushrooms, spinach and Parmesan cheese", "Spinach-mushroom", 30.0),
(18,false, "GARNISH", "02:02","Fried potatoes with roasted pork and onions", "Fried potatoes with frying", 3.0),
(19,false, "GARNISH", "02:02","Couscous with vegetables", "Couscous with vegetables", 17.3),
(20, true,"GARNISH", "02:02","Asparagus beans with cherry tomatoes", "Asparagus beans with cherry tomatoes", 9.0),

(21, false,"FRIED", "02:02","Baked salmon fillet, avocado, risotto with spinach, lemon sauce.", "BAKED SALMON WITH AVOCADO AND RICE", 22.2),
(22,false, "FRIED", "02:02","Lightly fried beef medallions are served with grilled vegetables such as mushrooms, zucchini, eggplant, peppers, corn, creamy mustard sauce.", "BEEF MEDALLIONS WITH GRILLED VEGETABLES", 10.0),
(23,false, "FRIED", "02:02","with salad mix, asparagus beans, bell peppers, cherry tomatoes, olives, pickled artichokes, lemon and dorbble sauce", "Grilled salmon fillet", 12.5),
(24,false, "FRIED", "02:02","Pork tenderloin, carrot puree, cherry tomatoes, salad, orange sauce", "PORK MEDALLIONS MADE OF CARROT MASH", 33.4),
(25,false, "FRIED", "02:02","Pork tenderloin combined with home-made potatoes with onions and bacon in a cheese sauce. Served with avocado, cherry", "PORK STEAK", 16.3),
(26, false,"FRIED", "02:02","Based on consomme and coconut cream, seasoned with cinnamon and spicy spices, salmon soaked in Japanese marinade, quail eggs, tofu cheese, Wakame seaweed and buckwheat noodles Soba.", "Consomme with salmon", 11.0),
(27, false,"FRIED", "02:02","Pork tenderloin, mashed potatoes, cherry tomatoes, salad, orange sauce", "PORK MEDALLIONS WITH MASHED POTATOES", 16.7),

(28,false, "DRINK", "02:02","wine is semi-sweet", "Sant'Orsola", 40.0),
(29,false, "DRINK", "02:02","Lemonade classic with mint", "Lemonade", 44.0),
(30,false, "DRINK", "02:02","Gold Polubotka - the only vodka in Ukraine with a content of 23-carat gold leaf 956", "Gold Polubotka", 48.0),
(31,false, "DRINK", "02:02","Grape varieties: Merlot, Cabernet Sauvignon, Cabernet Franc", "Wine Arnozan Bordeaux", 35.0),
(32,false, "DRINK", "02:02","Glenlivet whiskey, Nadurra Oloroso Matured 61.3% is made by traditional technologies developed in the middle of the 19th century.", "Whiskey The Glenlivet Nadurra Oloroso", 56.0),
(33,false, "DRINK", "02:02","Premium vodka Staritsky & Levitsky Reserve is born in an ecologically clean region of Ukraine - Prykarpattia. ", "Vodka Staritsky & Levitsky", 45.0),

(34,false, "MEAT", "02:02","Juicy veal on the bone, served with cauliflower puree, carrots pass on peas", "Veal squares", 8.0),
(35,false, "MEAT", "02:02","Minced meatballs in cream sauce and parmesan", "Chicken polpetti", 9.0),
(36,false, "MEAT", "02:02","Tender veal tenderloin with caramelized onions and mushrooms", "Fillet mignon", 5.0),
(37,false, "MEAT", "02:02","The most tender part of beef, baked and served in the author's sauce", "Chef's secret", 13.0),
(38,false, "MEAT", "02:02","Served with dor blue sauce with green beans", "Pork fillet", 14.0);
